<%--
  Created by IntelliJ IDEA.
  User: rlawj
  Date: 2019-06-15
  Time: 오후 7:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="DAO.UserDAO" %>
<%@ page import="java.io.PrintWriter" %>


<jsp:useBean id="user" class="DTO.User" scope="page"/>
<jsp:setProperty name="user" property="mid"/>
<jsp:setProperty name="user" property="mpw"/>
<html>
<head>

</head>
<body>
    <%
        UserDAO userDAO = new UserDAO();//인스턴스 생성
        int result = userDAO.login(user.getMid(),user.getMpw());

        //로그인 성공
        if(result == 1){
            session.setAttribute("mid",user.getMid());
            PrintWriter script = response.getWriter();
            script.println("<script>");
            script.println("location.href='../../index.jsp'");
            script.println("</script>");
        }


        //로그인 실패
        else if (result == 0) {
            PrintWriter script = response.getWriter();
            script.println("<script>");
            script.println("alert('비밀번호가 틀립니다.')");
            script.println("location.href='../../index.jsp'");
            script.println("</script>");
        }
        else if(result == -1){
            System.out.println("DB연동실패");
            PrintWriter script = response.getWriter();
            script.println("<script");
            script.println("alert(DB오류가 발생하였습니다");
            script.println("history.back()");
            script.println("/script>");
        }
    %>
</body>
</html>
