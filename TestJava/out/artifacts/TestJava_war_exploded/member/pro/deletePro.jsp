<%@ page import="java.io.PrintWriter" %>
<%@ page import="DTO.Bbs" %>
<%@ page import="DAO.BBSDAO" %><%--
  Created by IntelliJ IDEA.
  User: rlawj
  Date: 2019-06-19
  Time: 오후 10:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    request.setCharacterEncoding("UTF-8");
%>
<html>
<head>
    <title>Title</title>
</head>
<body>

<%
    String mid=null;
    if(session.getAttribute("mid")!=null){
        mid=(String)session.getAttribute("mid");
    }

    if(mid == null){
        PrintWriter script=response.getWriter();
        script.println("<script>");
        script.println("alert('로그인을 하세요.')");
        script.println("location.href='../view/LoginForm'");
        script.println("</script>");
    }
    int bbsID=0;
    if(request.getParameter("bbsID")!=null){
        bbsID=Integer.parseInt(request.getParameter("bbsID"));
    }

    if(bbsID==0){
        PrintWriter script=response.getWriter();
        script.println("<script>");
        script.println("alert('유효하지않은 글 입니다.')");
        script.println("location.href='../view/bbs.jsp'");
        script.println("</script>");
    }
    Bbs bbs=new BBSDAO().getBbs(bbsID);
    if(!mid.equals(bbs.getUserID())){
        PrintWriter script= response.getWriter();
        script.println("<script>");
        script.println("alert('권한이없습니다.')");
        script.println("location.href='../view/bbs.jsp'");
        script.println("</script>");
    }
    else{
        BBSDAO bbsDAO = new BBSDAO();
        int result=bbsDAO.delete(bbsID);
        if(result==-1){
            PrintWriter script=response.getWriter();
            script.println("<script>");
            script.println("alert('글삭제에 실패하였습니다.')");
            script.println("history.back()");
            script.println("</script>");
        }else{
            PrintWriter script=response.getWriter();
            script.println("<script>");
            script.println("location.href='../view/bbs.jsp'");
            script.println("</script>");
        }
    }

    %>
</body>
</html>
