<%@ page import="java.io.PrintWriter" %>
<%@ page import="DAO.BBSDAO" %><%--
  Created by IntelliJ IDEA.
  User: rlawj
  Date: 2019-06-19
  Time: 오후 5:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    request.setCharacterEncoding("UTF-8");
    response.setContentType("text/html; charset=UTF-8");

%>

<!--한명의 회원정보를 담는 user클래스를 java beans사용 /socope:페이지 현재의 페이지에서만 사용-->

<jsp:useBean id="bbs" class="DTO.Bbs" scope="page"/>
<!-- // Bbs bbs = new Bbs(); -->
<jsp:setProperty name="bbs" property="bbsTitle"/><!--bbs.setBbsTitle(requrst)-->
<jsp:setProperty name="bbs" property="bbsContent"/>
<%
    System.out.println(bbs);
%>


<html>
<head>
    <title>Title</title>
</head>
<body>

<%
    String mid=null;
    if (session.getAttribute("mid")!=null){ //유저아이디이름으로 세션이 존재하는 회원들은
        mid=(String)session.getAttribute("mid"); //유저아이디에 해당 세션값을 넣어준다
    }
    if (mid == null) {

        PrintWriter script =response.getWriter();
        script.println("<script>");
        script.println("alert('로그인을 하세요.')");
        script.println("location.href='../view/LoginForm.jsp'");
        script.println("</script>");

    }else{
        if(bbs.getBbsTitle() ==null || bbs.getBbsContent()==null){
            PrintWriter script=response.getWriter();
            script.println("<script>");
            script.println("alert('입력이 안된 사항이 있습니다.')");
            script.println("history.back()");
            script.println("</script>");
        }else{
            BBSDAO BbsDAO =new BBSDAO();
            int result = BbsDAO.write(bbs.getBbsTitle(),mid,bbs.getBbsContent());
            if(result==-1){
                PrintWriter script = response.getWriter();
                script.println("<script>");
                script.println("alert('글쓰기에 실패했습니다')");
                script.println("history.back()");
                script.println("</script>");

            }else{
                PrintWriter script =response.getWriter();
                script.println("<script>");
                script.println("location.href='../view/bbs.jsp'");
                script.println("</script>");
            }

        }
    }

%>





</body>
</html>
