<%--
  Created by IntelliJ IDEA.
  User: rlawj
  Date: 2019-06-15
  Time: 오후 7:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
    <%
        request.setCharacterEncoding("euc-kr");
    %>

    <style>
        form{
            width:50%;
            margin:auto;
        }
        .wrap{
            width:530px;
            margin-left:auto;
            margin-right:auto;
            text-align:center;
        }

        table{
            border:3px solid skyblue
        }

        td{
            border:1px solid skyblue
        }

        .title{
            background-color:skyblue
        }
    </style>


</head>

<body>
    <!-- 왼쪽, 오른쪽 바깥여백을 auto로 주면 중앙정렬된다.  -->
    <div class="wrap">
        <br><br>
        <b><font size="6" color="gray">회원가입</font></b>
        <br><br><br>

        <form action="../pro/joinPro.jsp" >
            <table>
                <tr>
                    <td class="title">아이디</td>
                    <td>
                        <input type="text" name="mid"  id="mid" maxlength="20">
                    </td>
                </tr>

                <tr>
                    <td class="title">비밀번호</td>
                    <td>
                        <input type="password" name="mpw" id="mpw" maxlength="15">
                    </td>
                </tr>
                <tr>
                    <td class="title">이름</td>
                    <td>
                        <input type="text" name="mname" id="mname" maxlength="40">
                    </td>
                </tr>

                <tr>
                    <td class="title">전화번호</td>
                    <td>
                        <input type="text" name="mphone" id="mphone" maxlength="40">
                    </td>
                </tr>

                <tr>
                    <td class="title">이메일</td>
                    <td>
                        <input type="text" name="memail"  id="memail" maxlength="40">
                    </td>
                </tr>
            </table>
            <br>
            <input type="submit" value="가입"/>
            <input type="button" value="취소" onclick="cancel()">
        </form>
    </div>
    <script>

        function cancel() {
            location.href='../../index.jsp';
        }
    </script>


</body>
</html>
