package DAO;
import DTO.User;

import java.sql.*;

public class UserDAO {


    //dao : 데이터베이스 접근약자로서
//
    private Connection conn;
    private PreparedStatement pstmt;
    private ResultSet rs;

    //mysql에 접속해주는 부분
    public UserDAO(){
        try{
            String url = "jdbc:mysql://localhost:3306/jsp?serverTimezone=UTC"+"&useUnicode=true"+"&characterEncoding=utf-8";
            String user = "root";
            String pw = "1234";

            Class.forName("com.mysql.cj.jdbc.Driver");
            conn=DriverManager.getConnection(url,user,pw);
            System.out.println("driver 연결성공");
        }catch(Exception e){
            e.printStackTrace();

        }
    }


//로그인을 시도하는 함수
public int login(String mid,String mpw) {
        System.out.println("UserDAO 접속!");
        String SQL="SELECT mpw FROM testdb WHERE mid=?";
        System.out.println("SELECT 문으로 아이디 , 비번찾음!");
        System.out.println("mpw: " +mpw+" id: "+mid);
        try {
            //pstmt:prepared statement 정해진 sql문장을 db에 삽입하는 형식으로 인스턴스가져옴
            pstmt=conn.prepareStatement(SQL);
            //sql인젝션같은 해킹기법을 방어하는것..pstmt을 이용해 하나의 문장을 미리준비해서(물음표사용)
            //물음표해당하는 내용을 유저아이디로,매개변수로 이용..1)존재하는지 2)비밀번호 무엇인지
            pstmt.setString(1,mid);
            System.out.println(1+","+mid+"ㅋㅋ.");
            //rs:result set에 결과보관
            rs=pstmt.executeQuery();
            //결과가 존재한다면 실행
            if(rs.next()) {
                //패스워드 일치한다면 실행
                if (rs.getString(1).equals(mpw)) {
                    System.out.println("DAO로그인성공");
                    return 1; //로그인성공
                }
            }else {
                System.out.println("DAO로그인실패");

                return 0; //비밀번호 불일치
            }
            return -1; // 아이디없음 ㄹ오류
        }catch(Exception e) {
            System.out.println("DB실패 == 오류내용 : ");
            e.printStackTrace();
        }
        return -2; // 데이터베이스 오류를 의미
    }


    public int join(User user){
        String SQL= "INSERT INTO testdb VALUES(?,?,?,?,?)";
        try{
            pstmt=conn.prepareStatement(SQL);
            pstmt.setString(1,user.getMid());
            pstmt.setString(2,user.getMpw());
            pstmt.setString(3,user.getMname());
            pstmt.setString(4,user.getMphone());
            pstmt.setString(5,user.getMemail());

            return pstmt.executeUpdate();
        }catch(Exception e){
            e.printStackTrace();
        }
        return -1; //DB오류
    }
}


