
<%@ page import="java.io.PrintWriter" %>
<%@ page import="DAO.UserDAO" %>
<%--
  Created by IntelliJ IDEA.
  User: rlawk
  Date: 2019-06-15
  Time: 오후 7:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"

%>


<% request.setCharacterEncoding("UTF-8"); %>

<!-- 한명의 회원정보를 담는 user클래스를 자바 빈즈로 사용 /scope:페이지 현재의 페이지에서만 사용-->
<jsp:useBean id="user" class="DTO.User" scope="page"/>
<jsp:setProperty name="user" property="mid"/>
<jsp:setProperty name="user" property="mpw"/>
<jsp:setProperty name="user" property="mname"/>
<jsp:setProperty name="user" property="mphone"/>
<jsp:setProperty name="user" property="memail"/>



<html>
<head>
    <title>join Pro</title>


</head>
<body>

<%

    //로그인된 회원들은 페이지에 접속 할수없도록하기
    String mid=null;
    if(session.getAttribute("mid")!=null){
        mid =(String)session.getAttribute("mid");
    }
    if(mid != null){
        PrintWriter script=response.getWriter();
        script.println("<script>");
        script.println("alert('이미 로그인 되어있습니다')");
        script.println("location.href='../../index'");
        script.println("<script>");
    }
    if(user.getMid()== null || user.getMpw()==null || user.getMname()==null ||
                user.getMphone()==null || user.getMemail() ==null){
        PrintWriter script = response.getWriter();
        script.println("<script>");
        script.println("alert('입력이 안 된 사항이 있습니다.')");
        script.println("history.back()");
        script.println("</script>");
    }else{
        UserDAO userDAO=new UserDAO();//인스턴스생성
        int result = userDAO.join(user);

        if(result == -1 ){ // 아이디가 기콘기기. 중복되면 오류
            PrintWriter script= response.getWriter();
            script.println("<script>");
            script.println("alert('이미 존재하는 아이디입니다.')");
            script.println("history.back()");
            script.println("</script>");
        }else{ //가입성공
            PrintWriter script = response.getWriter();
            script.println("<script>");
            script.println("location.href='../view/LoginForm.jsp'");
            script.println("</script>");
        }
    }
%>



</body>
</html>
