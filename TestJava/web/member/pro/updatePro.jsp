<%@ page import="java.io.PrintWriter" %>
<%@ page import="DTO.Bbs" %>
<%@ page import="DAO.BBSDAO" %><%--
  Created by IntelliJ IDEA.
  User: rlawj
  Date: 2019-06-19
  Time: 오후 9:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    request.setCharacterEncoding("UTF-8");
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <%
        String mid=null;
        if(session.getAttribute("mid")!=null){
            mid=(String)session.getAttribute("userID");
        }
        if(mid==null){
            PrintWriter script =response.getWriter();
            script.println("<script>");
            script.println("alert('로그인을 하세요.')");
            script.println("location.href='../view/login.jsp'");
            script.println("</script>");
        }

        //글이 유효한지 판별
        int bbsID =0;
        if(request.getParameter("bbsID")!=null){
            bbsID=Integer.parseInt(request.getParameter("bbsID"));
        }

        if(bbsID ==0){
            PrintWriter script =response.getWriter();
            script.println("<script>");
            script.println("alert('유효하지 않은 글입니다.')");
            script.println("location.href='../view/LoginForm.jsp'");
            script.println("</script>");
        }
        Bbs bbs=new BBSDAO().getBbs(bbsID);
        if(!mid.equals(bbs.getUserID())){
            PrintWriter script = response.getWriter();
            script.println("<script>");
            script.println("alert('권한이 없습니다.')");
            script.println("location.href='../view/LoginForm.jsp'");
            script.println("</script>");
        }else{
            if(request.getParameter("bbsTitle")==null || request.getParameter("bbsContent")==null
                    || request.getParameter("bbsTItle").equals("")|| request.getParameter("bbsContent").equals("")){

                PrintWriter script=response.getWriter();
                script.println("<script>");
                script.println("alert('입력이 안된사항이 있습니다.')");
                script.println("history.back()");
                script.println("</script>");
            }else{
                BBSDAO BbsDAO = new BBSDAO();
                int result=BbsDAO.update(bbsID,request.getParameter("bbsTitle"),request.getParameter("bbsContent"));

                if(result== -1){
                    PrintWriter script=response.getWriter();
                    script.println("<script>");
                    script.println("alert('글수정에 실패했습니다')");
                    script.println("history.back()");
                    script.println("</script>");
                }else{
                    PrintWriter script =response.getWriter();
                    script.println("<script>");
                    script.println("location.href='bbs.jsp'");
                    script.println("</script>");
                }
            }
        }
    %>
</body>
</html>
