<%@ page import="DAO.BBSDAO" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="DTO.Bbs" %><%--
  Created by IntelliJ IDEA.
  User: rlawj
  Date: 2019-06-19
  Time: 오후 4:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">

</head>
<body>
<%
    //로긴한사람이라면	 userID라는 변수에 해당 아이디가 담기고 그렇지 않으면 null값
    String mid = null;
    if (session.getAttribute("mid") != null) {
        mid = (String) session.getAttribute("mid");

    }

    int pageNumber=1;//기본페이지 넘버

    //페이지넘버값이 있을때
    if(request.getParameter("pageNumber")!=null){
        pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
    }
%>
<div class="container">
    <div class = "row">
        <table class="table table-striped" style="text-align:center; border:1px solid #dddddd">
            <thead>
            <tr>
                <th style="background-color: #eeeeee; text-align: center;">번호</th>
                <th style="background-color: #eeeeee; text-align: center;">제목</th>
                <th style="background-color: #eeeeee; text-align: center;">작성자</th>
                <th style="background-color: #eeeeee; text-align: center;">작성일</th>
            </tr>
            </thead>
            <tbody>
            <%
                BBSDAO bbsDAO = new BBSDAO();
                ArrayList<Bbs> list=bbsDAO.getList(pageNumber);
                for (int i=0; i<list.size();i++){
            %>
            <tr>
                <td><%= list.get(i).getBbsID() %></td>
                <td><a href="view.jsp?bbsID=<%=list.get(i).getBbsID()%>">
                    <%= list.get(i).getBbsTitle()%> </a></td>
                <td><%= list.get(i).getUserID()%></td>
                <td><%= list.get(i).getBbsDate().substring(0,11)+
                list.get(i).getBbsDate().substring(11,13)+"시"
                +list.get(i).getBbsDate().substring(14,16)+"분"%></td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
        <%
            if(pageNumber!=1){

        %>
        <a href="bbs.jsp?pageNumber=<%=pageNumber - 1%>" class="btn btn-success btn-arrow-left">이전</a>
        <%
            }
            if(bbsDAO.nextPage(pageNumber)){
        %>
        <a href="bbs.jsp?pageNumber=<%=pageNumber + 1%>" class="btn btn-success btn-arrow-left">다음</a>
        <%
            }
        %>

        <!-- 회원만 넘어가도록-->
        <%
            if(session.getAttribute("mid")!=null){
        %>
        <a href = "write.jsp" class="btn btn-primary pull-right">글쓰기</a>
        <%
            }else {
        %>
        <button class="btn btn-primary pull-right"
                onclick="if(confirm('로그인 하세요'))location.href='LoginForm.jsp';" type="button">글쓰기</button>
        <%
            }
        %>

    </div>
</div>



</body>
</html>
<!-- 애니매이션 담당 JQUERY -->

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

<!-- 부트스트랩 JS  -->

<script src="../../js/bootstrap.js"></script>
