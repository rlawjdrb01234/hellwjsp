<%@ page import="java.io.PrintWriter" %>
<%@ page import="DTO.Bbs" %>
<%@ page import="DAO.BBSDAO" %><%--
  Created by IntelliJ IDEA.
  User: rlawj
  Date: 2019-06-19
  Time: 오후 9:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">
</head>
<body>
<%
    //로긴한사람이라면	 mid라는 변수에 해당 아이디가 담기고 그렇지 않으면 null값
    String mid = null;
    if (session.getAttribute("mid") != null) {
        mid = (String) session.getAttribute("mid");

    }

    //로그인 안한경우
    if(mid==null) {
        PrintWriter script = response.getWriter();
        script.println("<script>");
        script.println("alert('로그인을 하세요')");
        script.println("location.href='LoginForm.jsp'");
        script.println("</script>");
    }

    int bbsID =0;
    if (request.getParameter("bbsID") != null) {
        bbsID=Integer.parseInt(request.getParameter("bbsID"));
    }

    if(bbsID==0){
        PrintWriter script = response.getWriter();
        script.println("<script>");
        script.println("alert('유효하지 않은 글 입니다.')");
        script.println("location.href='bbs.jsp'");
        script.println("</script>");
    }
    Bbs bbs = new BBSDAO().getBbs(bbsID);
    if(!mid.equals(bbs.getUserID())){
        PrintWriter script =response.getWriter();
        script.println("<script>");
        script.println("alert('권한이 없습니다.')");
        script.println("location.href='bbs.jsp'");
        script.println("</script>");
    }


%>


<!-- 게시판 -->

<div class="container">

    <div class="row">

        <form method="post" action="updatePro.jsp?bbsID=<%= bbsID %> ">

            <table class="table table-striped"

                   style="text-align: center; border: 1px solid #dddddd">

                <thead>

                <tr>

                    <th colspan="2"

                        style="background-color: #eeeeee; text-align: center;">글

                        수정 </th>

                </tr>

                </thead>

                <tbody>

                <tr>

                    <td><input type="text" class="form-control" placeholder="글 제목" name="bbsTitle" maxlength="50" value="<%= bbs.getBbsTitle() %>" ></td>

                </tr>

                <tr>

                    <td><textarea class="form-control" placeholder="글 내용" name="bbsContent" maxlength="2048" style="height: 350px;" ><%= bbs.getBbsContent() %></textarea></td>

                </tr>

                </tbody>

            </table>

            <button type="submit" class="btn btn-primary pull-right" >글수정</button>

        </form>

    </div>

</div>










<!-- 애니매이션 담당 JQUERY -->

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

<!-- 부트스트랩 JS  -->

<script src="js/bootstrap.js"></script>
</body>
</html>
