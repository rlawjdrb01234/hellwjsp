<%@ page import="java.io.PrintWriter" %>
<%@ page import="DTO.Bbs" %>
<%@ page import="DAO.BBSDAO" %><%--
  Created by IntelliJ IDEA.
  User: rlawj
  Date: 2019-06-19
  Time: 오후 7:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">

</head>
<body>
    <%
        //로긴한사람이라면	 userID라는 변수에 해당 아이디가 담기고 그렇지 않으면 null값
        String mid = null;
        if (session.getAttribute("mid") != null) {
            mid = (String) session.getAttribute("mid");

        }
        int bbsID = 0;
        if (request.getParameter("bbsID") != null) {
            bbsID = Integer.parseInt(request.getParameter("bbsID"));
        }
        if (bbsID == 0) {
            PrintWriter script = response.getWriter();
            script.println("<script>");
            script.println("alert('유효하지 않은 글 입니다.')");
            script.println("location.href = 'bbs.jsp'");
            script.println("</script>");
        }

        Bbs bbs = new BBSDAO().getBbs(bbsID);

    %>
    <!-- 게시판 -->

    <div class="container">

        <div class="row">
            <table class="table table-striped"
                   style="text-align: center; border: 1px solid #dddddd">
                <thead>
                <tr>
                    <th colspan="3"
                        style="background-color: #eeeeee; text-align: center;">글 보기 </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="width: 20%;"> 글 제목 </td>
                    <td colspan="2"><%= bbs.getBbsTitle() %></td>
                </tr>
                <tr>
                    <td>작성자</td>
                    <td colspan="2"><%= bbs.getUserID() %></td>
                </tr>
                <tr>
                    <td>작성일</td>
                    <td colspan="2"><%= bbs.getBbsDate().substring(0, 11) + bbs.getBbsDate().substring(11, 13) + "시"
                            + bbs.getBbsDate().substring(14, 16) + "분"%></td>
                </tr>
                <tr>
                    <td>내용</td>
                    <td colspan="2" style="min-height: 200px; text-align: left;"><%= bbs.getBbsContent()%></td>
                    <!-- .replace("",
                            "&nbsp;").replaceAll("<","&lt;").replaceAll(">",
                            "&gt,").replaceAll("\n","<br>")  -->
                </tr>
                </tbody>
            </table>
            <a href = "bbs.jsp" class="btn btn-primary">목록</a>
            <%
                //글작성자 본인일시 수정 삭제 가능
                if(mid != null && mid.equals(bbs.getUserID())){
            %>
            <a href="updateForm.jsp?bbsID=<%= bbsID %>" class="btn btn-primary">수정</a>
            <a onclick="return confirm('정말로 삭제하시겠습니까?')"
               href="../pro/deletePro.jsp?bbsID=<%= bbsID %>" class="btn btn-primary">삭제</a>
            <%
                }

            %>
        </div>
    </div>
    <!-- 애니매이션 담당 JQUERY -->

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

    <!-- 부트스트랩 JS  -->

    <script src="js/bootstrap.js"></script>

</body>
</html>


